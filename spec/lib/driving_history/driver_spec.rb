require_relative '../../../lib/driving_history/driver'
require_relative '../../../lib/driving_history/trip'

RSpec.describe DrivingHistory::Driver do
  context '#name' do
    it 'will return the name' do
      name = 'Ringo'

      driver = DrivingHistory::Driver.new(name)

      expect(driver.name).to eq(name)
    end
  end

  context '#total_trip_distance' do
    before(:example) do
      @driver = DrivingHistory::Driver.new('Ringo')

      @distances = [12.3, 0, 15]
      @trips     = @distances.each_with_index.collect do |distance, i|
        instance_double(
            DrivingHistory::Trip,
            "trip_#{i}",
            distance: distance,
            duration: 1.0
        )
      end
    end

    it 'will return zero when no trips have been added' do
      expect(@driver.total_trip_distance).to be_zero
    end

    it 'will return the total distance for a single trip' do
      trip           = @trips.first
      total_distance = @distances.first

      expect(trip).to receive(:distance)
      @driver.add_trip(trip)

      expect(@driver.total_trip_distance).to be_within(DELTA).of(total_distance)
    end

    it 'will return the total distance for multiple trips' do
      @trips.each do |trip|
        expect(trip).to receive(:distance)
        @driver.add_trip(trip)
      end
      total_distance = @distances.reduce(&:+)

      expect(@driver.total_trip_distance).to be_within(DELTA).of(total_distance)
    end

    it 'will recalculate total distance after adding a trip' do
      expect(@trips.first).to receive(:distance).at_most(2).times
      @driver.add_trip(@trips.first)
      @driver.total_trip_distance

      @trips[1..-1].each do |trip|
        expect(trip).to receive(:distance).once
        @driver.add_trip(trip)
      end
      total_distance = @distances.reduce(&:+)

      expect(@driver.total_trip_distance).to be_within(DELTA).of(total_distance)
    end
  end

  context '#total_trip_duration' do
    before(:example) do
      @driver = DrivingHistory::Driver.new('Ringo')

      @durations = [12.3, 0, 15]
      @trips     = @durations.each_with_index.collect do |duration, i|
        instance_double(
            DrivingHistory::Trip,
            "trip_#{i}",
            distance: 30,
            duration: duration
        )
      end
    end

    it 'will return zero when no trips have been added' do
      expect(@driver.total_trip_duration).to be_zero
    end

    it 'will return the total duration for a single trip' do
      trip           = @trips.first
      total_duration = @durations.first

      expect(trip).to receive(:duration)
      @driver.add_trip(trip)

      expect(@driver.total_trip_duration).to be_within(DELTA).of(total_duration)
    end

    it 'will return the total duration for multiple trips' do
      @trips.each do |trip|
        expect(trip).to receive(:duration)
        @driver.add_trip(trip)
      end
      total_duration = @durations.reduce(&:+)

      expect(@driver.total_trip_duration).to be_within(DELTA).of(total_duration)
    end

    it 'will recalculate total duration after adding a trip' do
      expect(@trips.first).to receive(:duration).at_most(2).times
      @driver.add_trip(@trips.first)
      @driver.total_trip_duration

      @trips[1..-1].each do |trip|
        expect(trip).to receive(:duration).once
        @driver.add_trip(trip)
      end
      total_duration = @durations.reduce(&:+)

      expect(@driver.total_trip_duration).to be_within(DELTA).of(total_duration)
    end
  end

  context '#average_trip_speed' do
    before(:example) do
      @driver = DrivingHistory::Driver.new('Ringo')

      @durations = [3600, 0, 1800]
      @distances = [12.3, 0, 15]
      @trips     = @durations.zip(@distances).each_with_index.collect do |(duration, distance), i|
        trip = instance_double(
            DrivingHistory::Trip,
            "trip_#{i}",
            duration: duration,
            distance: distance
        )
      end
    end

    it 'will return zero when no trips have been added' do
      expect(@driver.average_trip_speed).to be_zero
    end

    it 'will return zero when all trips have zero duration' do
      trip = instance_double(
          DrivingHistory::Trip,
          'trip_0',
          duration: 0,
          distance: 10.0
      )

      expect(trip).to receive(:duration)
      @driver.add_trip(trip)

      expect(@driver.average_trip_speed).to be_zero
    end

    it 'will return the average speed for a single trip' do
      trip          = @trips.first
      average_speed = @distances.first / @durations.first

      expect(trip).to receive(:duration)
      expect(trip).to receive(:distance)
      @driver.add_trip(trip)

      expect(@driver.average_trip_speed).to be_within(DELTA).of(average_speed)
    end

    it 'will return the average speed for multiple trips' do
      @trips.each do |trip|
        expect(trip).to receive(:duration)
        expect(trip).to receive(:distance)
        @driver.add_trip(trip)
      end

      average_trip_speed = @distances.reduce(&:+) / @durations.reduce(&:+)

      expect(@driver.average_trip_speed).to be_within(DELTA).of(average_trip_speed)
    end
  end

  context '::parse' do
    it 'will parse a driver command' do
      name = 'Ringo'

      driver = DrivingHistory::Driver.parse(name)

      expect(driver.name).to eq(name)
    end
  end
end
