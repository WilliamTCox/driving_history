require_relative '../../../lib/driving_history/history'
require_relative '../../../lib/driving_history/driver'
require_relative '../../../lib/driving_history/trip'

RSpec.describe DrivingHistory::History do
  context '#drivers' do
    before(:example) do
      @history = DrivingHistory::History.new
    end
    it 'will return an empty array when no drivers have been added' do
      expect(@history.drivers).to be_empty
    end
  end

  context '#add_driver' do
    before(:example) do
      @history = DrivingHistory::History.new

      @driver_names = %w[Ringo George Paul]
      @drivers      = @driver_names.collect do |name|
        instance_double(
            DrivingHistory::Driver,
            "driver_#{name}",
            name: name
        )
      end
    end

    it 'will return the drivers when one driver has been added' do
      driver = @drivers.first

      @history.add_driver(driver)

      expect(@history.drivers).to contain_exactly(driver)
    end

    it 'will return multiple drivers when multiple drivers have been added' do
      @drivers.each { |driver| @history.add_driver(driver) }

      expect(@history.drivers).to match_array(@drivers)
    end
  end

  context '#add_trip' do
    before(:example) do
      @speed_bounds = 5..100
      @history      = DrivingHistory::History.new(speed_bounds: @speed_bounds)

      @driver_names = %w[Ringo George Paul]
      @drivers      = @driver_names.collect do |name|
        instance_double(
            DrivingHistory::Driver,
            "driver_#{name}",
            name: name
        )
      end

      @drivers.each { |driver| @history.add_driver(driver) }

      @average_speed = (@speed_bounds.last - @speed_bounds.first) / 2.0
      @trip = instance_double(
          DrivingHistory::Trip,
          'trip',
          average_speed: @average_speed
      )
    end

    it 'will add the trip to the named driver' do
      driver = @drivers[1]

      expect(driver).to receive(:add_trip).with(@trip)
      @history.add_trip(driver.name, @trip)
    end

    it 'will add trips within the speed bounds' do
      driver = @drivers.first
      expect(driver).to receive(:add_trip).with(@trip)

      @history.add_trip(driver.name, @trip)
    end

    it 'will not add trips less than the speed bounds' do
      average_speed = @speed_bounds.first - 1
      trip          = instance_double(
          DrivingHistory::Trip,
          'trip',
          average_speed: average_speed
      )

      driver = @drivers.first
      expect(driver).to_not receive(:add_trip).with(trip)

      @history.add_trip(driver.name, trip)
    end

    it 'will not add trips greater than the speed bounds' do
      average_speed = @speed_bounds.last + 1
      trip          = instance_double(
          DrivingHistory::Trip,
          'trip',
          average_speed: average_speed
      )

      driver = @drivers.first
      expect(driver).to_not receive(:add_trip).with(trip)

      @history.add_trip(driver.name, trip)
    end
  end

  context '#parse' do
    before(:example) do
      @history = DrivingHistory::History.new
    end

    it 'will parse history data with ::parse_line' do
      lines = [
          'Driver Ringo',
          'Trip Ringo 12:00 13:00 30'
      ]

      data         = instance_double(IO)
      receive_each = lines.reduce(receive(:each).once) { |r, l| r.and_yield(l) }
      expect(data).to receive_each

      lines.each do |line|
        expect(@history).to receive(:parse_line).with(*line.split).ordered
      end

      @history.parse(data)
    end
  end

  context '#parse_line' do
    before(:example) do
      @driver_name = 'Ringo'
      @driver      = instance_double(
          DrivingHistory::Driver,
          'driver',
          name: @driver_name
      )

      @trip_start_time = '17:53'
      @trip_end_time   = '18:35'
      @trip_distance   = '30.2'
      @trip            = instance_double(
          DrivingHistory::Trip,
          'trip'
      )

      @history = DrivingHistory::History.new
    end

    it 'will parse a driver command' do
      command = ['Driver', @driver_name]

      expect(DrivingHistory::Driver).to receive(:parse).
          with(@driver_name).
          and_return(@driver).
          once
      expect(@history).to receive(:add_driver).
          with(@driver).
          once

      @history.parse_line(*command)
    end

    it 'will parse a trip command' do
      command = ['Trip', @driver_name, @trip_start_time, @trip_end_time, @trip_distance]

      expect(DrivingHistory::Trip).to receive(:parse).
          with(@trip_start_time, @trip_end_time, @trip_distance).
          and_return(@trip).
          once
      expect(@history).to receive(:add_trip).
          with(@driver_name, @trip).
          once

      @history.parse_line(*command)
    end
  end
end
