require_relative '../../../lib/driving_history/trip'

RSpec.describe DrivingHistory::Trip do
  HOURS_TO_MINUTES = 60.0
  MINUTES_TO_HOURS = 1.0 / HOURS_TO_MINUTES

  context '#distance' do
    it 'will return the distance' do
      duration = 1.0
      distance = 12.3

      trip = DrivingHistory::Trip.new(duration, distance)

      expect(trip.distance).to be_within(DELTA).of(distance)
    end
  end

  context '#duration' do
    it 'will return the duration' do
      duration = 1.0
      distance = 12.3

      trip = DrivingHistory::Trip.new(duration, distance)

      expect(trip.distance).to be_within(DELTA).of(distance)
    end
  end

  context '#average_speed' do
    before(:example) do
      @duration      = 2.0
      @distance      = 24.6
      @average_speed = @distance / @duration
    end

    it 'will return the average speed' do
      trip = DrivingHistory::Trip.new(@duration, @distance)

      expect(trip.average_speed).to be_within(DELTA).of(@average_speed)
    end

    it 'will return zero when the duration is zero' do
      @duration      = 0
      @average_speed = 0

      trip = DrivingHistory::Trip.new(@duration, @distance)

      expect(trip.average_speed).to be_zero
    end
  end

  context '::parse' do
    before(:example) do
      @start_time = '11:13'
      @end_time   = '13:15'
      @duration   = 2 + 2 * MINUTES_TO_HOURS
      @distance   = '55.1'
    end

    it 'will parse a trip command' do
      command = [@start_time, @end_time, @distance]

      trip = DrivingHistory::Trip.parse(*command)

      expect(trip.duration).to be_within(DELTA).of(@duration.to_f)
      expect(trip.distance).to be_within(DELTA).of(@distance.to_f)
    end

    it 'will parse a trip command with zero duration' do
      @start_time = '08:23'
      @end_time   = '08:23'
      @duration   = 0

      command = [@start_time, @end_time, @distance]

      trip = DrivingHistory::Trip.parse(*command)

      expect(trip.duration).to be_within(DELTA).of(@duration.to_f)
      expect(trip.distance).to be_within(DELTA).of(@distance.to_f)
    end

    it 'will parse a trip command with zero distance' do
      @distance = '0'

      command = [@start_time, @end_time, @distance]

      trip = DrivingHistory::Trip.parse(*command)

      expect(trip.duration).to be_within(DELTA).of(@duration.to_f)
      expect(trip.distance).to be_within(DELTA).of(@distance.to_f)
    end
  end
end
