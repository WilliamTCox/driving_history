require 'stringio'

require_relative '../../../lib/driving_history/report'
require_relative '../../../lib/driving_history/history'
require_relative '../../../lib/driving_history/driver'

RSpec.describe DrivingHistory::Report do
  context '#print_summary' do
    before(:example) do
      @drivers = []

      @history = instance_double(
          DrivingHistory::History,
          'history',
          drivers: @drivers
      )
    end

    it 'will provide an empty report when there are no drivers' do
      report = DrivingHistory::Report.new(@history)

      output = StringIO.new
      report.print_summary(output)

      expect(output.string.chomp).to be_empty
    end

    it 'will sort multiple drivers based on distance' do
      @drivers << instance_double(
          DrivingHistory::Driver,
          'Driver_Ringo',
          name:                'Ringo',
          total_trip_duration: 10.0 / 20,
          total_trip_distance: 10,
          average_trip_speed:  20
      )
      @drivers << instance_double(
          DrivingHistory::Driver,
          'Driver_George',
          name:                'George',
          total_trip_duration: 40.0 / 20,
          total_trip_distance: 40,
          average_trip_speed:  20
      )
      report = DrivingHistory::Report.new(@history)

      expected_output = "George: 40 miles @ 20 mph\nRingo: 10 miles @ 20 mph\n"
      actual_output   = StringIO.new
      report.print_summary(actual_output)

      expect(actual_output.string).to eq(expected_output)
    end

    it 'will round miles driven to the nearest integer' do
      @drivers << instance_double(
          DrivingHistory::Driver,
          'Driver_Ringo',
          name:                'Ringo',
          total_trip_duration: 10.1 / 20,
          total_trip_distance: 10.1,
          average_trip_speed:  20
      )
      @drivers << instance_double(
          DrivingHistory::Driver,
          'Driver_George',
          name:                'George',
          total_trip_duration: 40.9 / 20,
          total_trip_distance: 40.9,
          average_trip_speed:  20
      )
      report = DrivingHistory::Report.new(@history)

      expected_output = "George: 41 miles @ 20 mph\nRingo: 10 miles @ 20 mph\n"
      actual_output   = StringIO.new
      report.print_summary(actual_output)

      expect(actual_output.string).to eq(expected_output)
    end

    it 'will round miles per hour to the nearest integer' do
      @drivers << instance_double(
          DrivingHistory::Driver,
          'Driver_Ringo',
          name:                'Ringo',
          total_trip_duration: 10 / 20.1,
          total_trip_distance: 10,
          average_trip_speed:  20.1
      )
      @drivers << instance_double(
          DrivingHistory::Driver,
          'Driver_George',
          name:                'George',
          total_trip_duration: 40 / 20.9,
          total_trip_distance: 40,
          average_trip_speed:  20.9
      )
      report = DrivingHistory::Report.new(@history)

      expected_output = "George: 40 miles @ 21 mph\nRingo: 10 miles @ 20 mph\n"
      actual_output   = StringIO.new
      report.print_summary(actual_output)

      expect(actual_output.string).to eq(expected_output)
    end

    it 'will not print a speed for drivers with no miles' do
      @drivers << instance_double(
          DrivingHistory::Driver,
          'Driver_Ringo',
          name:                'Ringo',
          total_trip_duration: 0,
          total_trip_distance: 0,
          average_trip_speed:  0
      )
      report = DrivingHistory::Report.new(@history)

      expected_output = "Ringo: 0 miles\n"
      actual_output   = StringIO.new
      report.print_summary(actual_output)

      expect(actual_output.string).to eq(expected_output)
    end
  end
end
