Feature: Test that the Driving History application creates a proper report

  Scenario: A basic report is generated from a history file
    Given there is basic history data
    When the history data is processed from a file
    Then a basic report has been created

  Scenario: A basic report is generated from history on standard input
    Given there is basic history data
    When the history data is processed from standard input
    Then a basic report has been created

  Scenario: Trips with speeds less than 5mph or greater than 100mph are not included in the report
    Given there is history data with trip speeds less than 5mph and greater than 100mph
    When the history data is processed from standard input
    Then trips with speeds less than 5mph or greater than 100mph are excluded from the report
