require 'json'
require 'open3'

module Testing
  module DrivingHistory
    DRIVING_HISTORY_BIN = 'bin/driving_history.rb'

    DRIVING_HISTORY_DATA    = 'features/support/driving_history_data.json'
    DRIVING_HISTORY_REPORTS = 'features/support/driving_history_reports.json'

    def self.load_json_file(filename)
      File.open(filename) do |file|
        JSON.parse(file.read, symbolize_names: true)
      end
    end

    def self.driving_history_data
      @driving_history_data ||= load_json_file(DRIVING_HISTORY_DATA)
    end

    def self.driving_history_reports
      @driving_history_reports ||= load_json_file(DRIVING_HISTORY_REPORTS)
    end

    def self.capture2(args: nil, stdin_data: nil)
      Open3.capture2("ruby #{DRIVING_HISTORY_BIN} #{args}", stdin_data: stdin_data)
    end
  end
end
