When('the history data is processed from a file') do
  @history_report, _ = Testing::DrivingHistory.capture2(args: @history_file)
end

When('the history data is processed from standard input') do
  history_data       = File.open(@history_file, &:read)
  @history_report, _ = Testing::DrivingHistory.capture2(stdin_data: history_data)
end

Given('there is basic history data') do
  @history_file = Testing::DrivingHistory.driving_history_data[:basic]
end

Then('a basic report has been created') do
  report_file     = Testing::DrivingHistory.driving_history_reports[:basic]
  expected_report = File.open(report_file, &:read)

  actual_report   = @history_report.lines.collect(&:chomp)
  expected_report = expected_report.lines.collect(&:chomp)
  expect(actual_report).to eq(expected_report)
end

Given('there is history data with trip speeds less than 5mph and greater than 100mph') do
  @history_file = Testing::DrivingHistory.driving_history_data[:speed_bounds]
end

Then('trips with speeds less than 5mph or greater than 100mph are excluded from the report') do
  report_file     = Testing::DrivingHistory.driving_history_reports[:speed_bounds]
  expected_report = File.open(report_file, &:read)

  actual_report   = @history_report.lines.collect(&:chomp)
  expected_report = expected_report.lines.collect(&:chomp)
  expect(actual_report).to eq(expected_report)
end
