require_relative '../lib/driving_history/history'
require_relative '../lib/driving_history/report'

MIN_SPEED = 5
MAX_SPEED = 100

history = DrivingHistory::History.new(speed_bounds: MIN_SPEED..MAX_SPEED)
history.parse(ARGF)

report = DrivingHistory::Report.new(history)
report.print_summary($stdout)
