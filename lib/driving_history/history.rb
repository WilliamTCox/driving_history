require_relative 'driver'
require_relative 'trip'

module DrivingHistory
  class History
    SPEED_BOUNDS = 0..Float::INFINITY

    def initialize(speed_bounds: SPEED_BOUNDS)
      @speed_bounds = speed_bounds
      @drivers      = {}
    end

    def drivers
      @drivers.values
    end

    def add_driver(driver)
      @drivers[driver.name] = driver
    end

    def add_trip(driver_name, trip)
      @drivers[driver_name].add_trip(trip) if @speed_bounds.include?(trip.average_speed)
    end

    def parse(data)
      data.each { |line| parse_line(*line.split) }
    end

    def parse_line(command, *args)
      case command
      when 'Driver'
        add_driver(Driver.parse(*args))
      when 'Trip'
        name = args.first
        trip_args = args[1..-1]
        add_trip(name, Trip.parse(*trip_args))
      else
        nil
      end
    end
  end
end
