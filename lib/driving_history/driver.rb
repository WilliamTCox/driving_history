module DrivingHistory
  class Driver
    attr_reader :name
    attr_reader :total_trip_duration
    attr_reader :total_trip_distance

    def initialize(name)
      @name  = name

      @total_trip_distance = 0.0
      @total_trip_duration = 0.0
    end

    def add_trip(trip)
      @total_trip_distance += trip.distance
      @total_trip_duration += trip.duration
    end

    def average_trip_speed
      total_trip_duration.zero? ? 0.0 : (total_trip_distance / total_trip_duration)
    end

    def self.parse(name)
      self.new(name)
    end
  end
end
