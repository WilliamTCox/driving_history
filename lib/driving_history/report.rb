module DrivingHistory
  class Report
    def initialize(history)
      @history = history
    end

    def print_summary(io)
      drivers = @history.drivers.sort_by { |driver| -driver.total_trip_distance }

      drivers.each do |driver|
        name     = driver.name
        duration = driver.total_trip_duration
        if duration.zero?
          io.puts("#{name}: 0 miles")
        else
          distance = driver.total_trip_distance.round
          speed    = driver.average_trip_speed.round
          io.puts("#{name}: #{distance} miles @ #{speed} mph")
        end
      end
    end
  end
end
