require 'date'

module DrivingHistory
  class Trip
    HOURS_TO_MINUTES   = 60.0
    MINUTES_TO_SECONDS = 60.0
    HOURS_TO_SECONDS   = HOURS_TO_MINUTES * MINUTES_TO_SECONDS
    SECONDS_TO_HOURS   = 1.0 / HOURS_TO_SECONDS

    TIME_FORMAT = '%H:%M'

    attr_reader :distance
    attr_reader :duration

    def initialize(duration, distance)
      @duration = duration
      @distance = distance
    end

    def average_speed
      @duration.zero? ? 0.0 : (@distance / @duration)
    end

    def self.parse(start_time_s, end_time_s, distance_s)
      start_time = DateTime.strptime(start_time_s, TIME_FORMAT).to_time
      end_time   = DateTime.strptime(end_time_s, TIME_FORMAT).to_time

      duration = (end_time - start_time) * SECONDS_TO_HOURS
      distance = distance_s.to_f

      self.new(duration, distance)
    end
  end
end
