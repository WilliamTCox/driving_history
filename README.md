# Driving History

This README covers the design decisions for the driving history application.  The requirements can be found at <https://gist.github.com/dan-manges/1e1854d0704cb9132b74>.


## Usage

The application may be invoked by providing the history input either through standard input:
```sh
ruby bin/driving_history.rb < features/support/data/input.txt
```
or by providing the filename as an argument: 
```sh
ruby bin/driving_history.rb features/support/data/input.txt
```
Allowing both is a common idiom for POSIX utilities and the effort to add the feature is minimal with Ruby's `ARGF`.


## Directory Structure

The project is organized into the following directories:
- `bin`: Contains the main application file, `driving_history.rb`, which is a small wrapper around the code in `lib`
- `features`: Cucumber tests covering the behavior of the application in `bin`
- `lib`: Classes and modules for the application
- `spec`: RSpec unit tests for classes and modules in `lib`


## Class Structure

The class structure closely follows the structure of a driving history input file with `Driver` and `Trip` commands.

Each command, as well as the history as a whole, is represented by its own class.  The data for each command maps easily to an object.

The classes also handle most of the parsing of the associated command.  This keeps the logic for each type of data in one place while not creating too many abstraction layers over the simple logic required for this data.

Internally, classes store distances in miles and durations in hours.  This makes the calculations for report generation simple at the cost of using different units from, for example, the `Time` class in the core library that operates on seconds.  Future requirements may make it worthwhile to revisit this decision.

### History

The entire history is stored in a `History` object.

Parsing the input starts here.  It tokenizes each command and uses enough information to organize the data within itself.  It then delegates the rest of the parsing to the class associated with the command.  For example, it takes the name from the `Trip` command to associate it with a `Driver`.  It then passes the start time, end time, and distance to the `Trip` class to handle.

Internally, it stores the `Driver` objects in a `Hash` keyed by the name to efficiently associate trips with drivers.  Externally, it provides this information in an `Array` for report generation.

While storing information, it is responsible for high level business logic like removing trips with speeds outside of the specified bounds.  Whether this is appropriate depends some on the future direction this application takes.  If the rules are general data cleansing that should be applied before any analysis, this is an ideal place to handle it before any consumers access the data.  If future consumers would want a more complete dataset, this logic would be better left to that consumer (in this case, the `Report` class).


### Driver

Represents a driver in the history.

Parsing is simple as it only contains a name.

The external interface does not provide any means to remove or access individual trips, only ways to add or query aggregate data.  Since we only access average trip data, the `Driver` does not store individual trips and instead just accumulates the information as trips are added.

This allows for O(1) storage.  It also avoids issues around invalidating memoized calculations or O(n) calculations when the aggregating methods are called.

However, future requirements (e.g. median calculations or updating individual trips) may require storing `Trip` objects.


### Trip

Represents a trip taken by a driver.

The class itself only stores a duration and a distance.  Thus, parsing consists mostly of converting the start and end time to a duration in hours.

It knows nothing of the driver it is associated with.  In a relational database it would make sense to have the `Trip` object reference the `Driver` it is associated with, but in an object oriented language this is more naturally expressed by the `Driver` owning `Trip` objects (or discarding them, as discussed with the `Driver` class).


### Report

This class generates reports for the given `History`.

Currently, it only generates the summary specified in the requirements, but, in the future, it could provide different analyses of the data.


## Dependencies

The only external dependencies are on the Cucumber and RSpec gems for testing.
